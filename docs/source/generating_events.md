# Generating events : ART part


## Selecting the initial move

The first step in launching an event is to select the type of deformation for the initial move. 

This move can either be taken as a random displacement of all atoms in the box ( `global`) or be localized in space, centered around one atom (`local`). In the latter case, a few options are possible : the radius of the spherical region around this selected atom that is also included in the initial random move (`RADIUS_INITIAL_DEFORMATION`) as well as whether the displacement is oriented in the negative z direction, for insertion into a surface that is perpendicular to this axis (`INITIAL_PUSH_DOWNWARDS`).

In general, it is not recommended to use a `global` initial random displacement as events are, by definition, localized in a solid system, far away from a phase transition so that a global deformation will tend to over sample a few events that are very susceptible to be activated. 

By default, therefore, you should select a `local` event. You must then select the radius of deformation. In general, this radius is select to include first neighbours, sometimes going to second and third. Since the perpendicular relaxation, after each push allows all atoms to relax, it is generally preferable to stick to first neighbours. 

    setenv TYPE_OF_EVENTS             local   # Initial move for events - global or local
    setenv RADIUS_INITIAL_DEFORMATION 3.0     # Cutoff for local-move (in angstroems)

As explained above, and in more details in the section on breaking detailed balance, it is also possible  to select an initial move downwards, to favor motion into a surface which is normal to +z. **This option is only applied when the central atom belows to an active species**. This is down with :

    setenv  INITIAL_PUSH_DOWNWARDS   .true.   # Push in the negative z direction 
                                              # (xy anything) (default: .false.)
This should be used with caution as it will then apply to all atom types in the box. 


## Other variables to explain:

    setenv SADDLE_PUSH_PARAM          0.1     # The fraction of the initial-saddle distance used to push saddle config. away from initial minimum (default: 0.1)
    setenv EIGENVALUE_THRESHOLD      -1.0     # Eigenvalue threshold for leaving basin

    setenv EXIT_FORCE_THRESHOLD       0.1    # Threshold for convergence at saddle point
    setenv FORCE_THRESHOLD_PERP_REL   0.05    # Threshold for perpendicular relaxation

    setenv FINE_EXIT_FORCE_THRESHOLD       0.05    # finner Threshold for convergence at saddle point 
    setenv FINE_FORCE_THRESHOLD_PERP_REL   0.01    # finner Threshold for perpendicular relaxation

    setenv MIN_NUMBER_KSTEPS          0       # Min. number of ksteps before calling lanczos
    setenv INCREMENT_SIZE             0.1     # Overall scale for the increment moves in activation

    setenv INITIAL_STEP_SIZE          0.50    # Size of initial displacement, in A
    setenv BASIN_FACTOR               3.00
    setenv MIN_NUMBER_KSTEPS          2       # Min. number of ksteps before calling lanczos
    setenv MAX_PERP_MOVES_BASIN       3       # Maximum number of perpendicular steps leaving basin
    setenv MAX_PERP_MOVES_ACTIV       20       # Maximum number of perpendicular steps during activation


## Activated Event generation: ART-nouveau

ART nouveau parameters (more).

EVENT analysis

## Refining low barrier events

To take into account the elastic deformations present in the system,
k-ART refines events with small activation barriers (and large
associated rates) on each atom sharing the same initial topology. The
program insures that at least the low barrier events with a cumulative
rate compromising 99.99% of the total rate are refined. These refined
events are associated with a specific atom and are called SPECIFIC
events. Each SPECIFIC event replaces the original GENERIC event.

The parameters
