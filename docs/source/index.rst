Welcome to kART documentation!
==============================

**kART** implement the kinetic Activation Relaxation Technique, initialy developed by F. El-Mellouhi, Laurent Lewis and Normand Mousseau.

kART is an off-lattice kinetic Monte Carlo method that used the Activation-Relaxation Technique nouveau to generate events and NAUTY, a code that does topological classification of graphs, to identify unique events and generate a catalog.

Access to code is freely available upon request to Normand Mousseau ( normand.mousseau@umontreal.ca ).

.. note::

    This documentation need to be updated to refect change in the recent integration of `artn-plugin`. Some variable name have changed related to ARTn. Parallel forces hase not been implemented. And some new variable need to be documented.

    If you have any questions, please contact us (eugene.sanscartier@umontreal.ca and normand.mousseau@umontreal.ca).


Contents
--------

.. toctree::
    :caption: INSTALLATION
    :maxdepth: 1

    download
    build
    executing


.. toctree::
    :caption: USAGE
    :maxdepth: 2

    user_guide
    generating_events
    tuning


.. toctree::
    :caption: OTHERS
    :maxdepth: 2

    lammps
    parallel
    output
    rerun
    units
    errors
    bibliography


.. toctree::
    :caption: TUTORIALS
    :maxdepth: 2

    tutorial_si_vac
    tutorial_file_number
