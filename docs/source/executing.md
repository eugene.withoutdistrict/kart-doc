# Executing k-ART

For the first run we'll need a minimum number of files. Others can be
added with various parameters described below.

1.  *KMC.sh* script initializes ***all the input parameters*** and
    launches the k-ART executable.

2.  *initial.conf* which contains the system's initial configuration
    (atomic positions)

3.  To launch k-ART on cluster computers using a job management system,
    a script is needed (as an example, we give a *jobfile.sh* file) to
    launch the simulation (serial or MPI versions). In this file you set
    up the number of processors needed and launch the *KMC.sh* script.

To use k-ART with LAMMPS supported version you will also need:

1.  *in.lammps* which describes the physical parameters used by the
    lammps instance (i.e. box size, potential used).

2.  *Si.sw* This is the value of the parameter of the SW potential. You
    can use any of the potential in the potential directory in lammps
    directory but make sure that the potential you want to use require
    to load new package from lammps and also modify the in.lammps by
    include the new pair style. Also, some potential may require a
    library file in addition to the parameter file. In particular, this
    is the case for the MEAM and ReaxFF potentials. For further
    information, refer to the lammps website.

In the directory `EXAMPLES`, you will find a number of examples -- with
and without lammps -- on which you can execute k-ART. Most represent a
single vacancy in a box of 511-atom Stillinger-Weber crystalline
silicon. There is also a test with Si-H, an alloy.

In most directories, you will find a launch file (`KMC.sh`) that can be
used in serial (type in terminal):

       ./KMC.sh

and in parallel simulations:

       mpirun -np 12 KMC.sh

## Changing the script from csh to bash

If *csh* or a *csh*-compatible shell is not installed in your cluster
machine (or you don't want to use it), you don't need to write an
auxiliary file with *export* command of *bash*. In your *jobfile.sh*,
simply change change

      mpiexec ./KMC.sh

by

      eval $(awk '{if($1=="setenv"){$1="export";$2=$2"="$3;$3="";sub(/#.*/,"");print}}' KMC.sh)
      mpiexec ~/bin/KMCART_exec

The first line should replace KMC "*setenv csh*\" stile by "*export
bash*\" (*awk* creates the new script and 'eval\" export the awk output
to bash). The second line corresponds to the executable in `KMC.sh`. It
strongly depends on the system installed to submit the jobs into the
cluster machine. For example this is another version for parallel
simulations:

     eval $(awk '{if($1=="setenv"){$1="export";$2=$2"="$3;$3="";sub(/#.*/,"");print}}' KMC.sh)
     mpirun -np 4 ../../SRC/${EXE}

On personal computers go to directory with the input and in the terminal
copy-paste the fist line and type the second one according to your
executable path. More details about the examples are given in the
`ReadMe` file in `EXAMPLES` directory.
