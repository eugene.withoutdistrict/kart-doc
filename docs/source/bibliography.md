# Bibliography

N. Mousseau and L.J Lewis, *Computer models for amorphous silicon
hydrides*, Phys. Rev. B **41**, (1990) 3702-3707.

H. Vocks, M.V. Chubynsky, G.T. Barkema and N. Mousseau, *Activated
sampling in complex materials at finite temperature: the
properly-obeying-probability activation-relaxation technique*, J. Chem.
Phys. **123**, 244707:1-10 (2005).

M.V. Chubynsky, H. Vocks, G.T. Barkema and N.Mousseau, *Exploiting
memory in event-based simulations*, J. Non. Cryst. Solid **352**,
4424-4429 (2006).

B. Puchala, M. L. Falk and K. Garikipati, *An energy basin finding
algorithm for kinetic Monte Carlo acceleration*, J. Chem. Phys. **132**,
134104 (2010).

F. El-Mellouhi, N. Mousseau and L. J. Lewis, *The Kinetic
Activation-Relaxation Technique: A Powerful Off-lattice On-the-fly
Kinetic Monte Carlo Algorithm*, Phys. Rev. B. **78**, 153202 (2008).

M.-C. Marinica, F. Willaime and N. Mousseau, *Energy landscape of small
clusters of self-interstitial dumbbells in iron*, Phys. Rev. B **83**,
094119, (2011).

J.-F. Joly, L. K. Béland, P. Brommer, F. El-Mellouhi and N. Mousseau,
*Optimization of the Kinetic Activation-Relaxation Technique, an
off-lattice and self-learning kinetic Monte-Carlo method*, Journal of
Physics: Conference Series Series **341**, 012007 (2012).

L. K. Béland, P. Brommer, F. El-Mellouhi, J.-F. Joly and N. Mousseau,
*The Kinetic Activation Relaxation Technique*, Phys. Rev. E **84**,
046704 (2011).

E. Machado-Charry, L. K. Béland, D. Caliste, L. Genovese, N. Mousseau
and P. Pochet, *Optimized energy landscape exploration using the ab
initio based ART-nouveau*, J. Chem Phys. **135**, 034102 (2011).

N. Mousseau, L. K. Béland, P. Brommer, J.-F. Joly, F. El-Mellouhi, E.
Machado-Charry, P. Pochet et M.-C. Marinica, *The Activation-Relaxation
Technique: ART nouveau and kinetic ART*, J. Atom., Mol. and Opt. Phys.
**2012**, 925278 (2012).

P. Brommer and N. Mousseau, *Comment on "Mechanism of Void Nucleation
and Growth in bcc Fe: Atomistic Simulations at Experimental Time
Scales"*, Phys. Rev. Lett. **108**, 219601 (2012)

J.-F. Joly, L. K. Béland, P. Brommer and N. Mousseau, *Contribution of
vacancies to relaxation in amorphous materials: A kinetic
activation-relaxation technique study*, Phys. Rev. B **87**, 144204
(2013)

L.K. Béland, Y. Anahory, D. Smeets, M. Guihard, P. Brommer, J.-F. Joly,
J.-C. Pothier, L. J. Lewis, N. Mousseau and F. Schiettekatte, *Replenish
and relax: explaining logarithmic annealing in disordered materials*,
Phys. Rev. Lett. **111**, 105502 (2013).

L. K. Béland and N. Mousseau, *Relaxation of ion-bombarded silicon
studied with the kinetic Activation-Relaxation Technique*, Phys. Rev. B.
**88**, 214201 (2013).

M. Trochet, L.K. Béland, J-F Joly P. Brommer and N. Mousseau, *Diffusion
of point defects in crystalline silicon using the kinetic
activation-relaxation technique method*, Phys. Rev. B. **91**, 224106
(2015).

O.A. Restrepo, N. Mousseau,F. El-Mellouhi, O. Bouhali, M. Trochet and C.
S. Becquart, *Diffusion properties of Fe-C systems studied by using
kinetic activation-relaxation technique* , Comput. Mat. Sc. **112**,
96-106 (2016).

N. Mousseau, L.K. Béland, P. Brommer, J-F Joly, G. K. N'Tsouaglo, O.A.
Restrepo and M. Trochet,, *Following atomistic kinetics on experimental
timescales with the kinetic Activation-Relaxation Technique*, Comput.
Mat. Sc. **100**, 111-123 (2015).
